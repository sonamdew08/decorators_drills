import time

def memoize(func):
    cache = {}
    def fib_fun(n):
        if n not in cache:
            cache[n] = func(n)
        return cache[n]
    return fib_fun

@memoize
def fib(n):
    print("Computing fib({})".format(n))
    if n in [0, 1]:
        return n
    return fib(n - 1) + fib(n - 2)

print(fib(10))
