import math
import time

def timer(func):
    def countdown(x):
        start = time.time()
        result = func(x)
        end = time.time()
        print("time taken to execute " + str(func) + " is " + str(end - start))
        return result
    return countdown

@timer
def sqrt(x):
    return math.sqrt(x)

@timer
def square(x):
    return x * x

sqrt(16)
square(16)
