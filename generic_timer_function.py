import time

def generic_timer(func):
    def countdown(*args, **kwargs):
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        print("time taken to execute " + str(func) + " is " + str(end - start))
        return result
    return countdown

@generic_timer
def add(x, y):
    return x + y

@generic_timer
def add3(x, y, z):
    return x + y + z

@generic_timer
def add_any(*args):
    return sum(args)

@generic_timer
def abs_add_any(*args, **kwargs):
    total = sum(args)
    if kwargs.get('abs') is True:
        return abs(total)
    return total

print(add(3, 4))
print(add3(10, 20, 30))
print(add_any(8, 6, 4, 8, 9, 12))
print(abs_add_any(-9, -6, -7, abs = True))

